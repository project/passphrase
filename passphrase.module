<?php
/**
 * @file
 * Creates custom passphrase form element.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function passphrase_form_user_admin_settings_alter(&$form, &$form_state) {
  $form['passphrase'] = array(
    '#type' => 'fieldset',
    '#title' => t('User form customizations'),
  );

  $form['passphrase']['passphrase_user_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable passphrase instead of password field.'),
    '#default_value' => variable_get('passphrase_user_form', TRUE),
  );

  $form['email_title']['#weight'] = 2;

  $form['email']['#weight'] = 2;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function passphrase_form_user_profile_form_alter(&$form, &$form_state) {
  if (variable_get('passphrase_user_form', TRUE)) {
    $form['account']['pass']['#type'] = 'passphrase_confirm';
    $form['account']['pass']['#size'] = 60;
    $form['account']['pass']['#description'] = t('<strong>Passphrase?</strong> Yes. Passphrases are easier to remember and more secure than traditional passwords. For example, try a group of words with spaces in between, or a sentence you know you’ll remember. <code>Correct horse battery staple</code> is a better passphrase than <code>r0b0tz26</code>.');
  }
}

/**
 * Implements hook_element_info().
 */
function passphrase_element_info() {
  $types['passphrase_confirm'] = array(
    '#input' => TRUE,
    '#process' => array('passphrase_confirm_form_process'),
    '#theme_wrappers' => array('form_element'),
  );

  return $types;
}

/**
 * Expand a password_confirm field into two text boxes.
 */
function passphrase_confirm_form_process($element) {
  $element['#attached']['js'] = array(drupal_get_path('module', 'passphrase') . '/js/passphrase.form.js');
  $element['pass1'] =  array(
    '#type' => 'password',
    '#title' => t('Passphrase'),
    '#value' => empty($element['#value']) ? NULL : $element['#value']['pass1'],
    '#required' => $element['#required'],
    '#attributes' => array('class' => array('passphrase-field')),
    '#size' => 60,
    '#maxlength' => 255,
  );
  $element['pass2'] =  array(
    '#type' => 'password',
    '#title' => t('Confirm passphrase'),
    '#value' => empty($element['#value']) ? NULL : $element['#value']['pass2'],
    '#required' => $element['#required'],
    '#attributes' => array('class' => array('passphrase-confirm')),
    '#size' => 60,
    '#maxlength' => 255,
  );
  $element['#element_validate'] = array('passphrase_confirm_validate');
  $element['#tree'] = TRUE;

  if (isset($element['#size'])) {
    $element['pass1']['#size'] = $element['pass2']['#size'] = $element['#size'];
  }

  return $element;
}

/**
 * Validates a password_confirm element.
 */
function passphrase_confirm_validate($element, &$element_state) {
  $pass1 = trim($element['pass1']['#value']);
  $pass2 = trim($element['pass2']['#value']);
  if (!empty($pass1) || !empty($pass2)) {
    if (strcmp($pass1, $pass2)) {
      form_error($element, t('The specified passphrases do not match.'));
    }
  }
  elseif ($element['#required'] && !empty($element_state['input'])) {
    form_error($element, t('Passphrase field is required.'));
  }

  // Passphrase field must be converted from a two-element array into a single
  // string regardless of validation results.
  form_set_value($element['pass1'], NULL, $element_state);
  form_set_value($element['pass2'], NULL, $element_state);
  form_set_value($element, $pass1, $element_state);

  return $element;

}